EESchema Schematic File Version 4
LIBS:esp8266_shield-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 1250 3850 2    60   Input ~ 0
RST
Text GLabel 1250 3950 2    60   Input ~ 0
ADCex
Text GLabel 1250 4050 2    60   Input ~ 0
GPIO16/D0
Text GLabel 2700 3850 0    60   Input ~ 0
TXD
Text GLabel 2700 3950 0    60   Input ~ 0
RXD
Text GLabel 2700 4050 0    60   Input ~ 0
GPIO5/D1
Text GLabel 2700 4150 0    60   Input ~ 0
GPIO4/D2
Text GLabel 2700 4250 0    60   Input ~ 0
GPIO0/D3
Text GLabel 2700 4350 0    60   Input ~ 0
GPIO2/D4
$Comp
L ESP8266-oldNumbers:ESP-12E U1
U 1 1 5A886D78
P 3300 2050
F 0 "U1" H 3300 1950 50  0001 C CNN
F 1 "ESP-12E" H 3300 2150 50  0000 C CNN
F 2 "esp8266-oldNumbers:ESP-12E" H 3300 2050 50  0001 C CNN
F 3 "" H 3300 2050 50  0001 C CNN
	1    3300 2050
	1    0    0    -1  
$EndComp
Text GLabel 4350 1750 2    60   Input ~ 0
TXD
Text GLabel 4350 1850 2    60   Input ~ 0
RXD
Text GLabel 4350 1950 2    60   Input ~ 0
GPIO5/D1
Text GLabel 4350 2050 2    60   Input ~ 0
GPIO4/D2
Text GLabel 2200 2350 0    60   Input ~ 0
GPIO13/D7
Text GLabel 2200 2250 0    60   Input ~ 0
GPIO12/D6
Text GLabel 2200 2150 0    60   Input ~ 0
GPIO14/D5
Text GLabel 2200 2050 0    60   Input ~ 0
GPIO16/D0
Text GLabel 2200 1850 0    60   Input ~ 0
ADC
Text GLabel 2350 1350 2    60   Input ~ 0
GPIO16/D0
$Comp
L power:GND #PWR04
U 1 1 5A888671
P 4450 2750
F 0 "#PWR04" H 4450 2500 50  0001 C CNN
F 1 "GND" H 4450 2600 50  0000 C CNN
F 2 "" H 4450 2750 50  0001 C CNN
F 3 "" H 4450 2750 50  0001 C CNN
	1    4450 2750
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR05
U 1 1 5A88887F
P 4850 2750
F 0 "#PWR05" H 4850 2600 50  0001 C CNN
F 1 "+3.3V" H 4850 2890 50  0000 C CNN
F 2 "" H 4850 2750 50  0001 C CNN
F 3 "" H 4850 2750 50  0001 C CNN
	1    4850 2750
	-1   0    0    1   
$EndComp
Text GLabel 5150 2250 2    60   Input ~ 0
GPIO2/D4
Text GLabel 5150 2150 2    60   Input ~ 0
GPIO0/D3
Text GLabel 5150 2050 2    60   Input ~ 0
FLASH
$Comp
L power:+3.3V #PWR06
U 1 1 5A8891F6
P 2100 2850
F 0 "#PWR06" H 2100 2700 50  0001 C CNN
F 1 "+3.3V" H 2100 2990 50  0000 C CNN
F 2 "" H 2100 2850 50  0001 C CNN
F 3 "" H 2100 2850 50  0001 C CNN
	1    2100 2850
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5A889243
P 2300 2850
F 0 "#PWR07" H 2300 2600 50  0001 C CNN
F 1 "GND" H 2300 2700 50  0000 C CNN
F 2 "" H 2300 2850 50  0001 C CNN
F 3 "" H 2300 2850 50  0001 C CNN
	1    2300 2850
	1    0    0    -1  
$EndComp
Text GLabel 2350 1200 2    60   Input ~ 0
RST
$Comp
L power:GND #PWR08
U 1 1 5A88A064
P 1750 1150
F 0 "#PWR08" H 1750 900 50  0001 C CNN
F 1 "GND" H 1750 1000 50  0000 C CNN
F 2 "" H 1750 1150 50  0001 C CNN
F 3 "" H 1750 1150 50  0001 C CNN
	1    1750 1150
	-1   0    0    1   
$EndComp
$Comp
L power:+3.3V #PWR09
U 1 1 5A88A11E
P 1350 1150
F 0 "#PWR09" H 1350 1000 50  0001 C CNN
F 1 "+3.3V" H 1350 1290 50  0000 C CNN
F 2 "" H 1350 1150 50  0001 C CNN
F 3 "" H 1350 1150 50  0001 C CNN
	1    1350 1150
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR010
U 1 1 5A88AA59
P 4950 3650
F 0 "#PWR010" H 4950 3500 50  0001 C CNN
F 1 "+3.3V" H 4950 3790 50  0000 C CNN
F 2 "" H 4950 3650 50  0001 C CNN
F 3 "" H 4950 3650 50  0001 C CNN
	1    4950 3650
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR011
U 1 1 5A88AA94
P 4450 3650
F 0 "#PWR011" H 4450 3400 50  0001 C CNN
F 1 "GND" H 4450 3500 50  0000 C CNN
F 2 "" H 4450 3650 50  0001 C CNN
F 3 "" H 4450 3650 50  0001 C CNN
	1    4450 3650
	0    1    1    0   
$EndComp
Text GLabel 4450 3750 0    60   Input ~ 0
TXD
Text GLabel 4450 3850 0    60   Input ~ 0
RXD
Text GLabel 4950 3750 2    60   Input ~ 0
FLASH
Text GLabel 4950 3850 2    60   Input ~ 0
RST
Text Notes 3900 4400 0    60   ~ 0
Programmierport:\nPins sind so belegt, dass man in der unteren Reihe\nnur eine Schnittstelle zur seriellen Kommunikation hat.\n
Text Notes 1150 5300 0    60   ~ 0
Aufsteckschnittstelle:\nKompatibel zu WemosD1 Mini, damit man\ndie Erweiterungsboard benutzen kann.\nAchtung, wir stellen hier generell keine 5V\nzur Verfügung.
Text GLabel 6450 2200 0    60   Input ~ 0
ADC
Text GLabel 6650 1750 1    60   Input ~ 0
ADCex
$Comp
L power:GND #PWR016
U 1 1 5A88A963
P 6650 2750
F 0 "#PWR016" H 6650 2500 50  0001 C CNN
F 1 "GND" H 6650 2600 50  0000 C CNN
F 2 "" H 6650 2750 50  0001 C CNN
F 3 "" H 6650 2750 50  0001 C CNN
	1    6650 2750
	1    0    0    -1  
$EndComp
Text GLabel 5150 2350 2    60   Input ~ 0
GPIO15/D8
Text GLabel 1250 4150 2    60   Input ~ 0
GPIO14/D5
Text GLabel 1250 4250 2    60   Input ~ 0
GPIO12/D6
Text GLabel 1250 4350 2    60   Input ~ 0
GPIO13/D7
Text GLabel 1250 4450 2    60   Input ~ 0
GPIO15/D8
$Comp
L power:GND #PWR0101
U 1 1 5A8B5966
P 2700 4450
F 0 "#PWR0101" H 2700 4200 50  0001 C CNN
F 1 "GND" H 2700 4300 50  0000 C CNN
F 2 "" H 2700 4450 50  0001 C CNN
F 3 "" H 2700 4450 50  0001 C CNN
	1    2700 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 1750 4350 1750
Wire Wire Line
	4200 1850 4350 1850
Wire Wire Line
	4200 1950 4350 1950
Wire Wire Line
	4200 2050 4350 2050
Wire Wire Line
	2200 1850 2400 1850
Wire Wire Line
	2200 2050 2400 2050
Wire Wire Line
	2200 2150 2400 2150
Wire Wire Line
	2200 2250 2400 2250
Wire Wire Line
	2200 2350 2400 2350
Wire Wire Line
	4550 2750 4550 2650
Wire Wire Line
	4200 2150 4950 2150
Wire Wire Line
	4950 2050 4950 2150
Wire Wire Line
	4750 2750 4850 2750
Connection ~ 4850 2750
Wire Wire Line
	4350 2750 4350 2450
Wire Wire Line
	4350 2450 4200 2450
Wire Wire Line
	4350 2750 4450 2750
Connection ~ 4450 2750
Connection ~ 4950 2150
Wire Wire Line
	5150 2050 4950 2050
Wire Wire Line
	2100 2450 2100 2850
Wire Wire Line
	2300 2850 2300 2750
Wire Wire Line
	2400 1950 1250 1950
Wire Wire Line
	1250 1950 1250 1750
Wire Wire Line
	1250 1450 1250 1250
Wire Wire Line
	1250 1250 1350 1250
Wire Wire Line
	1500 1250 1500 1450
Wire Wire Line
	2150 1450 2150 1350
Wire Wire Line
	2150 1350 2350 1350
Wire Wire Line
	2350 1200 1950 1200
Wire Wire Line
	1950 1200 1950 1750
Connection ~ 1950 1750
Wire Wire Line
	1350 1250 1350 1150
Connection ~ 1350 1250
Wire Wire Line
	1750 1450 1750 1150
Wire Wire Line
	6650 1750 6650 1850
Wire Wire Line
	6450 2200 6650 2200
Wire Wire Line
	6650 2150 6650 2200
Connection ~ 6650 2200
Wire Wire Line
	6650 2750 6650 2600
$Comp
L power:+3.3V #PWR0102
U 1 1 5A8B5FBC
P 1250 4550
F 0 "#PWR0102" H 1250 4400 50  0001 C CNN
F 1 "+3.3V" H 1250 4690 50  0000 C CNN
F 2 "" H 1250 4550 50  0001 C CNN
F 3 "" H 1250 4550 50  0001 C CNN
	1    1250 4550
	0    1    1    0   
$EndComp
Wire Wire Line
	4850 2750 4950 2750
Wire Wire Line
	4450 2750 4550 2750
Wire Wire Line
	4950 2150 5150 2150
Wire Wire Line
	1350 1250 1500 1250
Wire Wire Line
	6650 2200 6650 2300
Wire Wire Line
	4200 2350 4550 2350
Wire Wire Line
	2100 2450 2300 2450
Wire Wire Line
	1500 1750 1750 1750
Wire Wire Line
	1950 1750 2150 1750
$Comp
L Device:R R3
U 1 1 5CDD436C
P 2150 1600
F 0 "R3" H 2220 1646 50  0000 L CNN
F 1 "0" H 2220 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 2080 1600 50  0001 C CNN
F 3 "~" H 2150 1600 50  0001 C CNN
	1    2150 1600
	1    0    0    -1  
$EndComp
Connection ~ 2150 1750
Wire Wire Line
	2150 1750 2400 1750
$Comp
L Device:R R2
U 1 1 5CDD4A50
P 1500 1600
F 0 "R2" H 1570 1646 50  0000 L CNN
F 1 "12k" H 1570 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1430 1600 50  0001 C CNN
F 3 "~" H 1500 1600 50  0001 C CNN
	1    1500 1600
	1    0    0    -1  
$EndComp
Connection ~ 1750 1750
Wire Wire Line
	1750 1750 1950 1750
$Comp
L Device:R R1
U 1 1 5CDD5176
P 1250 1600
F 0 "R1" H 1320 1646 50  0000 L CNN
F 1 "12K" H 1320 1555 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 1180 1600 50  0001 C CNN
F 3 "~" H 1250 1600 50  0001 C CNN
	1    1250 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5CDD6706
P 1750 1600
F 0 "C1" H 1865 1646 50  0000 L CNN
F 1 "70pF" H 1865 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 1788 1450 50  0001 C CNN
F 3 "~" H 1750 1600 50  0001 C CNN
	1    1750 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 5CDD7808
P 2300 2600
F 0 "C2" H 2418 2646 50  0000 L CNN
F 1 "100uF" H 2418 2555 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.50mm" H 2338 2450 50  0001 C CNN
F 3 "~" H 2300 2600 50  0001 C CNN
	1    2300 2600
	1    0    0    -1  
$EndComp
Connection ~ 2300 2450
Wire Wire Line
	2300 2450 2400 2450
$Comp
L Device:R R6
U 1 1 5CDD83C9
P 4950 2550
F 0 "R6" H 5020 2596 50  0000 L CNN
F 1 "12K" H 5020 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4880 2550 50  0001 C CNN
F 3 "~" H 4950 2550 50  0001 C CNN
	1    4950 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5CDD8788
P 4750 2550
F 0 "R5" H 4820 2596 50  0000 L CNN
F 1 "12K" H 4820 2505 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4680 2550 50  0001 C CNN
F 3 "~" H 4750 2550 50  0001 C CNN
	1    4750 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 5CDD88F0
P 4550 2500
F 0 "R4" H 4620 2546 50  0000 L CNN
F 1 "12K" H 4620 2455 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 4480 2500 50  0001 C CNN
F 3 "~" H 4550 2500 50  0001 C CNN
	1    4550 2500
	1    0    0    -1  
$EndComp
Connection ~ 4550 2350
Wire Wire Line
	4200 2250 4750 2250
Wire Wire Line
	4750 2400 4750 2250
Connection ~ 4750 2250
Wire Wire Line
	4750 2250 5150 2250
Wire Wire Line
	4750 2700 4750 2750
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J3
U 1 1 5CDE2047
P 4650 3750
F 0 "J3" H 4700 4067 50  0000 C CNN
F 1 "Conn_02x03_Odd_Even" H 4700 3976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 4650 3750 50  0001 C CNN
F 3 "~" H 4650 3750 50  0001 C CNN
	1    4650 3750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x09 J1
U 1 1 5CDE36AF
P 2900 4250
F 0 "J1" H 2980 4292 50  0000 L CNN
F 1 "Conn_01x09" H 2980 4201 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 2900 4250 50  0001 C CNN
F 3 "~" H 2900 4250 50  0001 C CNN
	1    2900 4250
	1    0    0    -1  
$EndComp
Text GLabel 2700 4550 0    60   Input ~ 0
5V
$Comp
L Connector_Generic:Conn_01x09 J2
U 1 1 5CDE6017
P 1050 4250
F 0 "J2" H 968 4867 50  0000 C CNN
F 1 "Conn_01x09" H 968 4776 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x09_P2.54mm_Vertical" H 1050 4250 50  0001 C CNN
F 3 "~" H 1050 4250 50  0001 C CNN
	1    1050 4250
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5CDFCDD4
P 6650 2450
F 0 "R8" H 6720 2496 50  0000 L CNN
F 1 "100k" H 6720 2405 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6580 2450 50  0001 C CNN
F 3 "~" H 6650 2450 50  0001 C CNN
	1    6650 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5CDFD2CB
P 6650 2000
F 0 "R7" H 6720 2046 50  0000 L CNN
F 1 "220k" H 6720 1955 50  0000 L CNN
F 2 "Resistor_SMD:R_1206_3216Metric_Pad1.42x1.75mm_HandSolder" V 6580 2000 50  0001 C CNN
F 3 "~" H 6650 2000 50  0001 C CNN
	1    6650 2000
	1    0    0    -1  
$EndComp
Text GLabel 3250 2950 3    60   Input ~ 0
GPIO9-D11
Text GLabel 3350 2950 3    60   Input ~ 0
GPIO10-D12
Text GLabel 1250 4650 2    60   Input ~ 0
GPIO9-D11
Text GLabel 2700 4650 0    60   Input ~ 0
GPIO10-D12
Text GLabel 3700 1150 0    60   Input ~ 0
RST
$Comp
L Switch:SW_Push SW1
U 1 1 5CE26C3B
P 4000 1150
F 0 "SW1" H 4000 1435 50  0000 C CNN
F 1 "SW_Push" H 4000 1344 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_B3U-1000P" H 4000 1350 50  0001 C CNN
F 3 "Tactile Push Button Switch Momentary Tact SMD 3x6x2.5mm" H 4000 1350 50  0001 C CNN
	1    4000 1150
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5CE270BE
P 4300 1150
F 0 "#PWR0103" H 4300 900 50  0001 C CNN
F 1 "GND" H 4300 1000 50  0000 C CNN
F 2 "" H 4300 1150 50  0001 C CNN
F 3 "" H 4300 1150 50  0001 C CNN
	1    4300 1150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4200 1150 4300 1150
Wire Wire Line
	3700 1150 3800 1150
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5CE0D5CA
P 3800 5450
F 0 "J4" H 3908 5731 50  0000 C CNN
F 1 "Conn_01x04_Male" H 3908 5640 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 3800 5450 50  0001 C CNN
F 3 "~" H 3800 5450 50  0001 C CNN
	1    3800 5450
	1    0    0    -1  
$EndComp
Text GLabel 3050 2950 3    60   Input ~ 0
CSO
Text GLabel 3150 2950 3    60   Input ~ 0
MISO
Text GLabel 3450 2950 3    60   Input ~ 0
MOSI
Text GLabel 3550 2950 3    60   Input ~ 0
SCLK
Text GLabel 4000 5350 2    60   Input ~ 0
CSO
Text GLabel 4000 5450 2    60   Input ~ 0
MISO
Text GLabel 4000 5550 2    60   Input ~ 0
MOSI
Text GLabel 4000 5650 2    60   Input ~ 0
SCLK
$Comp
L Connector:TestPoint TP1
U 1 1 5CE1A51D
P 5550 1000
F 0 "TP1" V 5504 1188 50  0000 L CNN
F 1 "TestPoint" V 5595 1188 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 5750 1000 50  0001 C CNN
F 3 "~" H 5750 1000 50  0001 C CNN
	1    5550 1000
	0    1    1    0   
$EndComp
Text GLabel 5550 1000 0    60   Input ~ 0
ADC
$Comp
L Connector:TestPoint TP2
U 1 1 5CE1BAAE
P 5600 1200
F 0 "TP2" V 5554 1388 50  0000 L CNN
F 1 "TestPoint" V 5645 1388 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 5800 1200 50  0001 C CNN
F 3 "~" H 5800 1200 50  0001 C CNN
	1    5600 1200
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP3
U 1 1 5CE1C2F7
P 5600 1400
F 0 "TP3" V 5554 1588 50  0000 L CNN
F 1 "TestPoint" V 5645 1588 50  0000 L CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 5800 1400 50  0001 C CNN
F 3 "~" H 5800 1400 50  0001 C CNN
	1    5600 1400
	0    1    1    0   
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 5CE1DEC3
P 5600 1200
F 0 "#PWR0104" H 5600 1050 50  0001 C CNN
F 1 "+3.3V" H 5600 1340 50  0000 C CNN
F 2 "" H 5600 1200 50  0001 C CNN
F 3 "" H 5600 1200 50  0001 C CNN
	1    5600 1200
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5CE21702
P 5600 1400
F 0 "#PWR0105" H 5600 1150 50  0001 C CNN
F 1 "GND" H 5600 1250 50  0000 C CNN
F 2 "" H 5600 1400 50  0001 C CNN
F 3 "" H 5600 1400 50  0001 C CNN
	1    5600 1400
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 2350 5150 2350
Wire Wire Line
	4950 2150 4950 2400
Wire Wire Line
	4950 2700 4950 2750
$EndSCHEMATC
