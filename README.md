## ESP8266-Shield
ESP8266-Shield with basic resistors and capacitors to run the ESP8266.

__Features__:

- RM = 2.54 mm
- Additional pins for GPIO9 and GPIO10 (IO Index 11 and 12). ESP must be in dual-IO-mode!

![alt text][logo]

[logo]: pic/board.png "Board V1.1.0 - 3D View "
